# PiRelay
Fun with Raspberry pi and relay and lcd and ...<br><br>
Hard:<br>
  lcd: 1602a <br>
  relay board: Keyes funduino Ver.:4R1B (four relays)<br>
  Raspberry PI B+<br><br>
Soft:<br>
Require Adafruit_CharLCD (https://github.com/adafruit/Adafruit-Raspberry-Pi-Python-Code.git)  <br><br> 

Clone from git <br>
sudo git clone https://github.com/pliski/PiRelay.git <br> <br>

Update<br> 
sudo git pull https://github.com/pliski/PiRelay.git<br> <br> 

Execute<br> 
sudo python /home/pi/share/PiRelay/src/RelayLcdMain.py<br> <br> 

Execute inside directory<br> 
sudo python ./src/RelayLcdMain.py<br> <br> 
