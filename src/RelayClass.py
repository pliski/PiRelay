#!/usr/bin/python

class Relay(object):

    #costanti comuni a tutte le istanze
    OFF     = "OFF"
    ON      = "ON "

    def __init__(self, pin, GPIO):
        
        #TODO: ctrl che sia specificato un pin valido (cfr. con una lista di pin eligibili)
        if pin > 0 and pin < 28:
            
            self.PIN = pin

            #TODO: ctrl valori GPIO
            self.GPIO = GPIO
            self.GPIO.setup(self.PIN, self.GPIO.OUT) 
            self.setLOW()
        
        else:
            print ("\nError! Invalid pin value: " + str(pin))
            #TODO:gestire errore
                
               
        
    def setLOW(self):
        self.STATE = self.OFF        
        self.GPIO.output(self.PIN, self.GPIO.LOW)
        
    def setHIGH(self):
        self.STATE = self.ON
        self.GPIO.output(self.PIN, self.GPIO.HIGH)
        
    def toggle(self):
        if self.STATE == self.ON:
            self.setLOW()
        else:
            self.setHIGH()
                    
    def getState(self):
        return self.STATE
