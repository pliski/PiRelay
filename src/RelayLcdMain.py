#!/usr/bin/python
import sys
sys.path.append('/home/pi/share/Adafruit-Raspberry-Pi-Python-Code/Adafruit_CharLCD')

from Adafruit_CharLCD import Adafruit_CharLCD
from RelayClass import Relay
from subprocess import *
from time import sleep

import RPi.GPIO as GPIO

GPIO.setmode(GPIO.BCM)

lcd = Adafruit_CharLCD(26, 19, [13, 6, 5, 12], GPIO)
lcd.clear()

SleepTimeS = 0.4

# init list with pin numbers
pinList = [21, 23, 24, 25]
relayList = []

try:      

    for i in pinList: 
        rel = Relay(i, GPIO)
        relayList.append(rel)

    while 1:
        for rel in relayList:
            
            rel.toggle()

            msg = ""
            i=1
            for x in relayList:
                msg += "#R"+str(i)+" "+x.getState()+" "
                i += 1
                if i == 3:
                    msg += "\n"
                    
            print "\n"+ msg + "\n"
            
            lcd.clear()
            lcd.message(msg)
            sleep(SleepTimeS);
            
# End program cleanly with keyboard
except KeyboardInterrupt:
    print "  Quit"

    # Reset GPIO settings
    GPIO.cleanup()
